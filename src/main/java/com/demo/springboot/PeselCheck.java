package com.demo.springboot;

public class PeselCheck {
    private final byte[] PESEL = new byte[11];

    public boolean PeselCheckMethod(String peselNumber){
        if (peselNumber.length() != 11){
            return false;
        }else {
            for (int i = 0; i < 11; i++){
                PESEL[i] = Byte.parseByte(peselNumber.substring(i, i+1));
            }
            return sumCheck();
        }
    }

    private boolean sumCheck() {
        int sum = PESEL[0] +
                3 * PESEL[1] +
                7 * PESEL[2] +
                9 * PESEL[3] +
                PESEL[4] +
                3 * PESEL[5] +
                7 * PESEL[6] +
                9 * PESEL[7] +
                PESEL[8] +
                3 * PESEL[9];
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;

        return sum == PESEL[10];
    }
}
